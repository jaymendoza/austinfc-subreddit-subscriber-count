require 'bundler'

Bundler.require
Dotenv.load

mysql = Mysql2::Client.new(
  :host => "localhost",
  :username => ENV['DB_USER'],
  :password => ENV['DB_PASS']
)
mysql.query("USE austinfc")

session = Redd.it(
  user_agent: 'Redd:AustinFCTracker:v0.0.1 (by /u/TheesUhlmann)',
  client_id:  ENV['CLIENT_ID'],
  secret:     ENV['SECRET'],
  username:   'TheesUhlmann',
  password:   ENV['PASSWORD']
)

afc = session.subreddit('AustinFC')

subscribers = afc.subscribers
timestamp = Time.now.to_i

mysql.query("INSERT INTO subscribers (timestamp, subscribers, club) VALUES (#{timestamp}, #{subscribers}, 'Austin FC')")